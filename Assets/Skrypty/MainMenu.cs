﻿
using System.Text.RegularExpressions;
 using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Button = UnityEngine.UI.Button;



namespace Skrypty
{
    public class MainMenu : MonoBehaviour
    {
        #region Components

        public Button AprooveButton;
        public GameObject MaleAvatar;
        public GameObject FemaleAvatar;
        public InputField UserNameInput;
        public Toggle FemaleToggle;
        public Toggle MaleToggle;

        #endregion Components

        private Regex regex = new Regex(@"^[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]{3,25}$");
        public string PlayerName;
        public string Gender;
        public bool ClearPrefs;
        // public Text Kobieta;
        // public Text Meszczyzna;

        #region Init

        public void startNewGame()
        {
            SceneManager.LoadScene("Level1");
        }
        
        public void openPreferences()
        {
            SceneManager.LoadScene("Preferences");
        }
        
        public void quitGame()
        {
            Debug.Log("EXIT");
            Application.Quit();
        }

        void Update()
        {
            Debug.Log("TA FUNKCJA DZIALA");
        }

        void Start()
        {
            
            PlayerPrefs.DeleteAll(); // TODO remove it.
            ResetTaskPercentages();

   
            //PrepareView();

        }
        
        public void DebugLog()
        {
            Debug.Log("TA FUNKCJA DZIALA");
        }
        
        private void OnInitComplete()
        {
         
        }

        private void OnHideUnity(bool isGameShown)
        {

        }

        private void ResetTaskPercentages()
        {
            PlayerPrefs.SetInt("ProActivPoints", 0);
            PlayerPrefs.Save();
        }
        

        // private void PrepareView()
        // {
        //     ActiveButton(false);
        //
        //     if (string.IsNullOrEmpty(Gender))
        //         Gender = "female";
        //
        //     if (Gender == "female")
        //         FemaleToggle.isOn = true;
        //     else
        //         MaleToggle.isOn = true;
        //
        //     ToogleGender(Gender);
        //
        //     UserNameInput.text = PlayerName;
        //
        //     Validation();
        // }
        
        #endregion Init
        
        public void UserTextChanged()
        {
            PlayerName = UserNameInput.text;

            Validation();
        }

        void Validation()
        {
            if (regex.IsMatch(PlayerName) && Gender != "")
                ActiveButton(true);
            else
                ActiveButton(false);
        }
        void ActiveButton(bool state)
        {
            AprooveButton.interactable = state;
        }

        public void ToogleGender(string sex)
        {
            SetAvatar(sex);
            Gender = sex;
            
            // if (Gender != "male")
            // {
            //     Meszczyzna.fontStyle = FontStyle.Normal;
            //     Kobieta.fontStyle = FontStyle.Bold;
            // }else            {
            //     Meszczyzna.fontStyle = FontStyle.Bold;
            //     Kobieta.fontStyle = FontStyle.Normal;
            // }
        }
        void SetAvatar(string sex)
        {
            MaleAvatar.SetActive(sex == "male");
            FemaleAvatar.SetActive(sex == "female");
        }
        
        public void PlayGame()
        {
            // SaveData(Prefs.PlayerName, PlayerName);
            // SaveData(Prefs.Gender, Gender);
            // SaveData(Prefs.Progress, "0");
            SceneManager.LoadScene("MainMenu");
            PlayerPrefs.SetInt("PlayerSetup", 1);
            PlayerPrefs.Save();
        }
    }
}